# push homebrew binaries in the front of $PATH
export PATH=/usr/local/bin:$PATH
export PATH=/usr/local/sbin:$PATH

# push sdks in front of the path
export PATH=~/Library/SDKs/dart-20131114/dart-sdk/bin:$PATH

# override the default OSX ls settings
alias ls='ls -G'

# show the full path in the terminal
export PS1='\u@\h:\[\033[01;34m\]\w\[\033[00m\]\$ '
export CLICOLOR=1
