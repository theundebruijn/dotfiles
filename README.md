dotfiles
========

this repo contains my dotfiles. it's supposed to be pulled into your home dir. (the .gitignore file is especially important since it will exclude everything expect the highly necessary)

pull this repo into the root of your home dir (don't clone as it will create a dotfiles/ folder)

```
cd ~
git init
git remote add origin git@github.com:theundebruijn/dotfiles.git (make sure you have loaded the github keys via ssh-add)
git pull origin master
```

if, after pulling, the dotfiles get committed for removal, revert to the latest HEAD revision.

```
git reset --hard HEAD
```
